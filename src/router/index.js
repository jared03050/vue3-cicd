import { createRouter, createWebHashHistory } from 'vue-router';
import profile from '../components/profile'
import redirect from '../components/redirect'

import HelloWorld from '../components/HelloWorld.vue'

let queryHandler = function(){

  console.log('redirecting !');
  setTimeout(()=>{
    window.location.replace("https://www.google.com")
    // window.location.href = 'https://www.google.com'
  },2000)
}

const routes = [
  {
    path: '/',
    name: 'HelloWorld',
    component: HelloWorld,
  },
  {
    path: '/redirect',
    name: 'redirect',
    component: redirect,
    beforeEnter(to, from){
      console.log('to', to);
      console.log('from', from);
      queryHandler()
    }
  },
  {
    path: '/profile',
    name: 'profile',
    component: profile,
  },
  // {
  //   path: '*',
  //   redirect:'/'
  // },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
