import { shallowMount } from '@vue/test-utils';
import NavBar from '@/components/Nav.vue'


describe('navBar-test',()=>{
    // #1
    test('profile Link',()=>{
        const NavWrapper = shallowMount(NavBar);
        const profileLink = NavWrapper.get('#profile');
        expect(profileLink.text()).toBe('profile')
    })

    test('admin Link',()=>{
        const NavWrapper = shallowMount(NavBar,{
            props:{admin:true}
        });
        // wrong
        // const adminLink = NavWrapper.get('qwe123');
        
        // ok
        const adminLink = NavWrapper.get('#admin');
        expect(adminLink.text()).toBe('admin Page')

    })
})